<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/api/v1/getMenu', 'MenuController@getMenu');
Route::post('/api/v1/postOrder', 'MenuController@postOrder');
// https://www.dropbox.com/sh/kqylgaq5wjbn514/AABUJzoJ_0x_uIab4QNc2wJDa?dl=0
