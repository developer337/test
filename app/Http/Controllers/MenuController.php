<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use League\Flysystem\Filesystem;
use Spatie\Dropbox\Client;
use Spatie\FlysystemDropbox\DropboxAdapter;

class MenuController extends Controller
{
    public function getMenu()
    {
        return json_encode(Menu::all());
    }
    public function postOrder(Request $request)
    {
      $data = $request->all();
      $data = array_filter($data['number']);
      
      $client = new Client('W4MJp_cf_hAAAAAAAAAADicIeHl2Xb7kpaslbo_q8pf-r2JfQ8can9Em_xY2uy-l');
      $adapter = new DropboxAdapter($client);
      $filesystem = new Filesystem($adapter);

      $meta_data = $client->upload('/laravel/file_' . time() . '.txt', serialize($data));

      echo json_encode($meta_data);
    }
}
