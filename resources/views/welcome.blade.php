<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
            .element-name {
              display: inline-block;
              min-width: 250px;
            }
        </style>
    </head>
    <body>
      <div id="app">
        <h1>Menu</h1>
        <form class="js-submit-menuform">
          @csrf
          <div class="js-elements-container">
            loading...
          </div>
          <div><input type="submit" value="Order" name="submit" /></div>
        </form>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script>
      $(function(){
        // instant loading the elements
        $.ajax({
          method: "GET",
          url: "/api/v1/getMenu",
          dataType: "json"
        }).done(function( data ) {
          $('.js-elements-container').html('');
          $.each(data, function(i, item) {
            var html = '<div class="element">#' + item.id +' <span class="element-name">'+ item.name + '</span> ($' + item.cost + ')' + ' <input type="number" value="" min="1" max="999" name="number[' + item.id + ']" /></div>';
            $('.js-elements-container').append(html);
          });
        });
        // place an order Event
        $(document).on('submit', '.js-submit-menuform', function(e){
          var _this = $(this);
          $.ajax({
            method: "POST",
            url: "/api/v1/postOrder",
            data: _this.serialize(),
            dataType: "html"
          }).done(function( data ) {
            console.log(data);
            alert('success');
          });
          return false;
        });
      });

      </script>
    </body>
</html>
