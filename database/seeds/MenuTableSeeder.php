<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
            	foreach (range(1,30) as $index) {
        	        DB::table('menu')->insert([
        	            'name' => $faker->word,
        	            'cost' => $faker->numberBetween(500, 5000),
        	        ]);
        	}
    }
}
